import React, { Component } from 'react';
import { View, Text,Image, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
class Photo extends Component {
  constructor(props) {
    super(props);
    this.state = {
        // photo:require('./../Assets/logo.png'),
        photo:[
            {
                image:require('./../Assets/bur1.jpg')
            },
            {
                image:require('./../Assets/bur2.png')
            },
            {
                image:require('./../Assets/bur3.jpg')
            },
            {
                image:require('./../Assets/bur4.jpg')
            },
            {
                image:require('./../Assets/bur2.png')
            },
            {
                image:require('./../Assets/bur2.png')
            },
            {
                image:require('./../Assets/bur2.png')
            },
        ]
    };
  }


    RenderImage = ()=>{
           return this.state.photo.map((data,i)=>{
               return <View key={i}>
                   {
                       this.state.photo.length == 1 ?
                       <View style={{ width:'100%' }}>
                            <Image
                                style={{ width:'100%',height:200,resizeMode:'cover' }}
                                source={data.image}
                            />
                       </View>
                       :
                       this.state.photo.length == 2 ?
                       <View>
                           <View style={{ flexDirection:'row',justifyContent:'space-between',flexWrap:'wrap' }}>
                                <View style={{ width:'50%' }}>
                                    <Image
                                        style={{ width:'100%',height:200,resizeMode:'cover' }}
                                        source={this.state.photo[0].image}
                                    />
                                </View>
                                <View style={{ width:'50%' }}>
                                        <Image
                                            style={{ width:'100%',height:200,resizeMode:'cover' }}
                                            source={this.state.photo[1].image}
                                        />
                                </View>
                           </View> 
                       </View>
                       :
                       this.state.photo.length == 3 ?
                       <View>
                            <View style={{ flexDirection:'row',justifyContent:'space-between',flexWrap:'wrap' }}>
                                <View style={{ width:'50%' }}>
                                    <Image
                                        style={{ width:'100%',height:200,resizeMode:'cover' }}
                                        source={this.state.photo[0].image}
                                    />
                                </View>
                                <View style={{ width:'50%' }}>
                                        <Image
                                            style={{ width:'100%',height:200,resizeMode:'cover' }}
                                            source={this.state.photo[1].image}
                                        />
                                </View>
                                <View style={{ width:'100%' }}>
                                        <Image
                                            style={{ width:'100%',height:200,resizeMode:'cover' }}
                                            source={this.state.photo[2].image}
                                        />
                                </View>
                           </View>
                       </View>
                       :
                       this.state.photo.length == 4 ?
                       <View>
                           <View style={{ flexDirection:'row',justifyContent:'space-between',flexWrap:'wrap' }}>
                                <View style={{ width:'50%' }}>
                                    <Image
                                        style={{ width:'100%',height:200,resizeMode:'cover' }}
                                        source={this.state.photo[0].image}
                                    />
                                </View>
                                <View style={{ width:'50%' }}>
                                        <Image
                                            style={{ width:'100%',height:200,resizeMode:'cover' }}
                                            source={this.state.photo[1].image}
                                        />
                                </View>
                                <View style={{ width:'50%' }}>
                                        <Image
                                            style={{ width:'100%',height:200,resizeMode:'cover' }}
                                            source={this.state.photo[2].image}
                                        />
                                </View>
                                <View style={{ width:'50%',position:'relative' }}>
                                        <Image
                                            style={{ width:'100%',height:200,resizeMode:'cover' }}
                                            source={this.state.photo[3].image}
                                        />
                                        <View>
                                    </View>
                                </View>
                               
                           </View>
                       </View>
                       :
                       <View>
                       <View style={{ flexDirection:'row',justifyContent:'space-between',flexWrap:'wrap' }}>
                            <View style={{ width:'50%' }}>
                                <Image
                                    style={{ width:'100%',height:200,resizeMode:'cover' }}
                                    source={this.state.photo[0].image}
                                />
                            </View>
                            <View style={{ width:'50%' }}>
                                    <Image
                                        style={{ width:'100%',height:200,resizeMode:'cover' }}
                                        source={this.state.photo[1].image}
                                    />
                            </View>
                            <View style={{ width:'50%' }}>
                                    <Image
                                        style={{ width:'100%',height:200,resizeMode:'cover' }}
                                        source={this.state.photo[2].image}
                                    />
                            </View>
                            <View style={{ width:'50%', }}>
                                   <View>
                                        <Image
                                                style={{ width:'100%',height:200,resizeMode:'cover' }}
                                                source={this.state.photo[3].image}
                                            />
                                    </View>
                                    <View style={{ backgroundColor:'rgba(0,0,0,.7)',position:'absolute',justifyContent:'center',alignItems:'center',width:'100%',height:'100%',top:0,zIndex:999 }}>
                                    {
                                        this.state.photo.length > 4 ?
                                        <View style={{ }}>
                                            <Text style={{ color:'#fff' }}>{`${this.state.photo.length-4}+`}</Text>
                                        </View>
                                        :
                                        null
                                    }
                                    </View>
                            </View>
                           
                       </View>
                   </View>
                   }
               </View>
           })
    
        
    }


  render() {
    return (
      <View style={{ flex:1 }}>
        {
            this.RenderImage()
        }
      </View>
    );
  }
}

export default Photo;
